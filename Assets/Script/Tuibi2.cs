﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tuibi2 : MonoBehaviour
{
    //プレイヤーアタッチ
    public GameObject player;

    //玉の位置
    Vector3 tamapozi;

    //移動ON,OFF
    public bool sinnyuu = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (sinnyuu == true)
        {
            //玉の位置を現在位置へ
            tamapozi = this.transform.position;

            //プレイヤの位置-玉の位置
            Vector3 playiti = player.transform.position - tamapozi;

            //玉のリジッドボディに速度を与えてプレイヤーに追尾
            GetComponent<Rigidbody>().velocity = playiti * 300.0f * Time.deltaTime;

            //Debug.Log(player.transform.position);
        }
    }

    /*private void OnTriggerEnter(Collider other)
    {
        if (sinnyuu == false && other.tag == "Player")
        {
            //trueにする
            sinnyuu = true;

            Debug.Log("入った");
        }
    }*/

    public void Kirikae()
    {
        sinnyuu = true;
    }
}