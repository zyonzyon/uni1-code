﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tuibi : MonoBehaviour
{
    //プレイヤーアタッチ
    public GameObject player;

    //SpriteRenderer sr = null;

    //玉の位置
    Vector3 tamapozi;

    // Start is called before the first frame update
    void Start()
    {
        //sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        //玉の位置を現在位置へ
        tamapozi = this.transform.position;

        //プレイヤの位置-玉の位置
        Vector3 playiti = player.transform.position - tamapozi;

        //玉のリジッドボディに速度を与えてプレイヤーに追尾
        GetComponent<Rigidbody>().velocity = playiti * 3.0f * Time.deltaTime;

        //Debug.Log(player.transform.position);

        /*if (sr.isVisible)
        {
            Debug.Log("画面内");
        }*/
    }
}
