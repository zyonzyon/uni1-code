﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//シーン移動に必要
using UnityEngine.SceneManagement;

public class Bom : MonoBehaviour
{
    //エフェクトをアタッチ
    public GameObject detonator;

    //プレイヤーアタッチ
    public GameObject player;

    //箱のオブジェアタッチ
    public GameObject modbig;

    //最初はfalseにしとく
    bool hitflag = false;

    //効果音アタッチ
    AudioSource explosion;

    //死亡エフェクト用
    public GameObject siboueffect;
    public Vector3 effectpropa;

    // Start is called before the first frame update
    void Start()
    {

        explosion = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        //侵入したタグがプレイヤー、hitflagがfalseだったら
        if (hitflag == false && other.tag == "Player")
        {
            //trueにする
            hitflag = true;

            //第一引数=実行するメソッド、第二が秒数
            Invoke("BomCheck", 5f);

        }
    }

    //ゲームオブジェクトが存在するか確認する
    public void BomCheck()
    {
        // 存在する場合 ※オブジェクトの有効・無効は関係しない
        if (modbig)
        {

            //アタッチした効果音を出す
            explosion.Play();

            //アタッチしたエフェクトを起動
            Instantiate(
                detonator.gameObject,
                transform.position,
                Quaternion.identity
                );

            

            Destroy(player.gameObject);

            Instantiate(
                siboueffect,
                player.transform.position,
                Quaternion.Euler(effectpropa)
                );

            Invoke("Sini", 5f);
        }

        //時間内に消した場合
        else
        {
            Debug.Log("解除成功");
        }
    }

    public void Sini()
    {
        //()内は移動したいシーン名
        SceneManager.LoadScene("GameoverScene");
    }
}
