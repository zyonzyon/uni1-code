﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tuibi3 : MonoBehaviour
{
    //プレイヤーアタッチ
    public GameObject player;

    //オブジェアタッチ
    public GameObject sufia;

    //スクリプトを読み込み
    Tuibi2 script;

    // Start is called before the first frame update
    void Start()
    {
        //アタッチしたオブジェクトの名前が一致か確認する
        //sufia = GameObject.Find("idousphere");

        // スクリプトを格納
        script = sufia.GetComponent<Tuibi2>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            //アタッチしたオブジェクトのスクリプト<スクリプト名>、Kirieメソッドを使用
            //sufia.GetComponent<Tuibi2>().Kirikae();

            script.Kirikae();
        }
    }
}
