﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//シーン移動に必要
using UnityEngine.SceneManagement;

public class Playerdesu : MonoBehaviour
{
    //死亡エフェクト用
    public GameObject siboueffect;
    public Vector3 effectpropa;

    public GameObject player;

    //効果音アタッチ
    AudioSource sinioto;

    // Start is called before the first frame update
    void Start()
    {
     sinioto = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    /*void Update()
    {
        
    }*/

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
 
            //アタッチした効果音を出す
            sinioto.Play();

            Instantiate(
                siboueffect,
                other.transform.position,
                Quaternion.Euler(effectpropa)
                );


            Invoke("Otita", 5f);
        }
    }

    public void Otita()
    {
        //()内は移動したいシーン名
        SceneManager.LoadScene("GameoverScene");
    }
}
