﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SignBord : MonoBehaviour
{
    //シリアライゼーション、オブジェクトの状態などを再構成できる形式に変換する
    [SerializeField]
    public GameObject messagePref;

    private GameObject canvas;
    private GameObject messageUI;

    void Start()
    {
        //Cancasオブジェを検索
        canvas = GameObject.Find("Canvas");
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            //messageUIがない場合
            if (!messageUI)
            {
                // messageUiを作成
                messageUI = Instantiate(messagePref) as GameObject;

                // メッセージ設定
                Text messageUIText = messageUI.GetComponent<Text>();

            }
        }
    }

    //public IEnumerator OnTriggerExit(Collider other)
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (messageUI)
            {
                //yield return null;

                // メッセージ削除
                Destroy(messageUI);
            }
        }
    }
}