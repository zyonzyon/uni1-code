﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
	public int minute;

	public float seconds;

	//　前のUpdateの時の秒数
	private float oldSeconds;

	//　タイマー表示用テキスト
	public static Text timerText;

	void Start()
	{
		minute = 0;
		seconds = 0f;
		oldSeconds = 0f;
		timerText = GetComponentInChildren<Text>();

	}

	void Update()
	{
		//deltaTimeでフレームレートを合わせる
		seconds += Time.deltaTime;

		//秒が60以上だったら分カウントに加算
		if (seconds >= 60f)
		{
			minute++;
			seconds = seconds - 60;
		}
		//　値が変わった時だけテキストUIを更新
		if ((int)seconds != (int)oldSeconds)
		{
			//ToStringで文字列に変換
			timerText.text = minute.ToString("00") + ":" + ((int)seconds).ToString("00");
		}
		oldSeconds = seconds;


		//別のシーンで使う、分と秒をセット
		PlayerPrefs.SetInt("time",minute);

		PlayerPrefs.SetInt("time2", ((int)seconds));
	}
}