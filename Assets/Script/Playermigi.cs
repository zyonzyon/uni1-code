﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playermigi : MonoBehaviour
{
    //プレイヤー情報を取得
    public GameObject player;

    //プレイヤーの位置を取得
    Transform playiti;

    //プレイヤのVector3を入れる
    Vector3 playervec3;

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            //アタッチしたプレイヤーの現在地を入れる
            playiti = player.transform;

            //プレイヤーのpositionを入れる
            playervec3 = playiti.position;

            //プレイヤーのポジションXに値を加える
            playervec3.x += 0.01f;

            //プレイヤーの位置に反映させる
            playiti.position = playervec3;

        }
    }
}
