﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//シーン移動に必要
using UnityEngine.SceneManagement;

public class Gamecrea : MonoBehaviour
{

    public GameObject player;

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Invoke("Cria", 3f);
        }
    }

    public void Cria()
    {
        //()内は移動したいシーン名
        SceneManager.LoadScene("Gamecrea");
    }

}
