﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//UIとシーン移動に必要
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Startbutton : MonoBehaviour
{
    //ボタンを押したときの処理
    public void Onclick()
    {
        //()内は移動したいシーン名
        SceneManager.LoadScene("MainScene");
    }
}
